// ------------------------------------------------------
// filename:	RoutingTable.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		15.10.2003
// version:		20031015
// description: Implementation of a routers routing table
// ------------------------------------------------------

package routingtable;

import java.util.Vector;

import utility.Debug;
import message.RIPMessage;
import utility.Message;


public class RoutingTable {

	private Vector destinations;
	private Vector distances;
	private Vector nexthops;
	private String routerID;
	private Debug d;
	private Message msg;


	public RoutingTable(String routerID_){
		routerID 		= routerID_;
		d 				= new Debug("RoutingTable '"+routerID+"'");
		destinations	= new Vector();
		distances		= new Vector();
		nexthops		= new Vector();
		msg 			= new Message("RoutingTable '"+routerID+"'");
	}


	public void addEntry(RIPMessage ripMessage, String source) {
		int i;
		String destination;
		String distance;
		int dist;
		int insert = -1;
		int update = -1;

		
		for (i=0; i<ripMessage.getDestinations().size(); i++){
			destination = (String)ripMessage.getDestinations().get(i);
			distance = (String)ripMessage.getDistances().get(i);
			dist = Integer.parseInt(distance);
			//d.print("Destination: "+destination+" - Distance: "+distance);

			insert = -1;
			update = -1;
			
			// entry is not in table
			if (isNotInTable(destination)){
				insert = 0;
				//d.print("entry was not in table -> insert");
			// entry in table, with same source as this packages source
			} else if ( sameSource(destination, source)>-1 ){
				update = sameSource(destination, source);
				//d.print("entry in tabel w/same source as this new msg -> update");
			// entry in table (not same source) and distance is shorter than existing entry 
			} else if ( shorterDistance(destination,distance)>-1 ){
				update = shorterDistance(destination,distance);
				//d.print("entry in table, but this new distance is shorter -> update");
			// do nothing
			} else {
				//d.print("is in table w/ diff source and shorter distance, do nothing");
			}


			// won't add ourself
			if (destination.equals(routerID)){
				insert = -1;
				//d.print("uuups wait... dest = this.routerID -> don't insert");
			}


			// increase distance by one
			// since the distance to the neighbour is 1
			dist++;
			distance = Integer.toString(dist);
			if (distance.length()<2)
				distance = "0"+distance;


			if (insert>-1){
				destinations.add(ripMessage.getDestinations().get(i));
				distances.add(distance);
				nexthops.add(source);
			} else if (update>-1){

				// remove existing values
				destinations.remove(update);
				distances.remove(update);
				nexthops.remove(update);
				
				// add new values
				destinations.add(ripMessage.getDestinations().get(i));
				distances.add(distance);
				nexthops.add(source);
			}
		}
	}


	public Vector getDestinations(){
		return destinations;
	}


	public Vector getDistances(){
		return distances;
	}

	
	private boolean isNotInTable(String destination){
		int i;

		for (i=0; i<destinations.size(); i++){
			if (destination.equals((String)destinations.get(i)))
				return false;
		}
		return true;
	}


	// ------------------------------------
	// check that entry is in table, and if nexthop in table is equal
	// to the source (sender) of the package
	// ------------------------------------
	private int sameSource(String destination, String source){
		int i;

		for (i=0; i<destinations.size(); i++){
			if (destination.equals((String)destinations.get(i)))
				if (source.equals((String)nexthops.get(i)))
					return i;
		}
		return -1;
	}


	// ------------------------------------
	// check if the distance is shorter than existing entry...
	// ------------------------------------
	private int shorterDistance(String destination, String distance){
		int i;
		int dist = Integer.parseInt(distance);
		dist++; // have to add one to the distance, since the distance to the neighbour is 1

		for (i=0; i<destinations.size(); i++){
			if (destination.equals((String)destinations.get(i)))
				// if the new distance+1 is less than existing -> true
				if (dist < Integer.parseInt((String)distances.get(i)))
					return i;
		}
		return -1;
	}


	public void printTable(){
		int i;
		String output = "\n";
		boolean print = false;

		output += "------------------\n";
		output += "dest  | di | nexth\n";
		output += "------------------\n";

		for (i=0; i<destinations.size(); i++){
			output += (String)destinations.get(i)+" | "+(String)distances.get(i)+" | "+(String)nexthops.get(i)+"\n";
		}
		output += "------------------\n";
		
		if (print)
			d.print(output);		
	}


	public boolean isEmpty(){
		if (destinations.size()==0)
			return true;
		else
			return false;
	}


	public String getNextHop(String destination){
		int i;

		for (i=0; i<destinations.size(); i++){
			if (destination.equals((String)destinations.get(i)))
				return (String)destinations.get(i);
		}
		return null;
	}
}
