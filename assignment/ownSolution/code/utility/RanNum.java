// -------------------------------------------------------------------------
// filename:	RanNum.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		23.10.2003
// version:		20031023
// description: Generates a random number based on sysdate.
// -------------------------------------------------------------------------

package utility;

import java.util.Date;


public class RanNum {

	public RanNum(){}


	public long get(int length){
		Date date = new Date();
		long msec;
		long returnvalue;
		String number;

		msec = date.getTime();
		
		number = (new Long(msec)).toString();
		
		number = number.substring(number.length()-length, number.length());
		
		returnvalue = (new Long(number)).longValue();
		
		return returnvalue;
	}
}
