// -----------------------------------------------------------------------------------------------------
// filename:	BusyWait.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		23.10.2003
// version:		20031023
// description: Waiting implemented by doing "busy waiting". Not optimal (lock, semaphores is better) 
//				but easy to use.
// -----------------------------------------------------------------------------------------------------

package utility;

import java.util.Date;


public class BusyWait {

	public BusyWait(){}

	// busy waiting
	public static void doWait(long msec){
		Date now;
		long startTime;

		now = new Date();
		startTime = now.getTime();
		while (now.getTime() <= startTime + msec){
			now = new Date();
		}
	}
}
