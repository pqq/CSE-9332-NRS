// ------------------------------------------------------
// filename:	Debug.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		15.10.2003
// version:		20031015
// description: Prints out debugging messages
// ------------------------------------------------------

package utility;

import java.util.Date;

import utility.Parser;

public class Debug {
	private boolean debug;
	private boolean timestamp;
	private String referer;
	private String outputString;

	// Constructor
	public Debug(String referer_){
		referer			= referer_;
		outputString	= null;
		debug			= true;
		timestamp		= true;
	}


	// print out error message
	public void print (String string){
		if (debug){
			if (timestamp) {
				String timeStamp = (new Date()).toString();
				timeStamp = Parser.parse(timeStamp, " ")[3];
				outputString = "["+ referer + " "+timeStamp+"]: "+string;
			} else
				outputString = "["+ referer +"]: "+string;
				
			System.out.println(outputString);
		}
	}
}
