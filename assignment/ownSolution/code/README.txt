# -----------------------------------------------
# What:		MobileIPDemo - 9332 Assignment
# Author:	Frode Klevstul (frode@klevstul.com)
# -----------------------------------------------

----------
* How to run the demo
----------

Compile the files by writing

% javac MobileIPDemo.java

Run the program by writing

% java MobileIPDemo




----------
* Description of the different files / catalogues
----------

/
	MobileIPDemo.java
		This controls the entire demo, starts the different routers, nodes.
		Connect the nodes to the routers etc.

/config/

	Configuration files for the different routers. One file for each router.
	
/global/
	
	Not in use. (Planned to implement a global file with all constants).
	
/listener/

	The listener, used by the nodes. Listen to a given port.
	
/message/

	All different messages that is beeing sendt in between the nodes.
	
/mobileipregistration/

	Class to keep a mobile node connected to a foreign agent, re-register.
	
/node/

	All nodes in the system. Routers (basic router, foreign agent, home agent).
	Hosts (stationary and mobile).
	
/routingtable/

	A routers routingtable.
	
/task/

	Java Timer Tasks. Only have a periodic update, for sending routingtables
	in between routers.
	
/utility/

	Different utilities to parse tekst, debug etc.


------------
* Short walkthrough of the MobileIPDemo
------------

	We start up five different routers. Three foreign agents, one home agent,
	and one basic router.
	
	After starting up the different routers, we wait untill the routers will
	have time to exchange all routertable information in between them (routing 
	table construction and convergence using RIP).
	
	Then we start a mobile node, and a stationary node, connected to different
	routers.

	Then we start sending packages from the stationary node to the mobile node,
	while moving the mobile node around from one router to another. Each 
	datapackage from the stationary host is replied by an acknowledgement 
	package from the mobile host.


---------------------------
* Nodes, how they work
---------------------------
Every node is listening to one port on the local machin. To simulate sending
of data through the network, a package is sendt back to the same machine
(localhost). A nodes address ("IP address") is the same as the port the
node is listening on. When a node's listener is receiving a package, it
will be processed in the node, and appropriate action will be taken.


------------------
* Connecting a host to a router
------------------

When a host is connected to a router it receives a new IP message from the
router. Each router can have 0-9 nodes connected. The address given to the
node is "router address + # node connected".

Ex: Router has address "18010", and node 1 connects as the first node to 
this router, it will receive IP 18010 + 1 = 18011.

All routers' address ends with "0", all hosts' address ends with "1-9".

Next the router starts a listener on given address, and requests an
"Agent Discovery Message" (ADM) from the router, by sending an "Agent 
Discovery Request" (ADR) message. The content in the ADM message gives
the node information about the router (if it acts as a home agent, 
foreign agent).

If a node has been connected to a router before, it stops the listener
on that address before it connects to the new network.

If the node is a mobile node, and has been registered to FA, it sends
a de-registration message before it leaves the old router.

----------------------
* Default router topography
----------------------
HA = Home Agent
FA = Foreign Agent
MN = Mobile Node
CN = Stationary Node

The number (ex. 18010) specifies the address of the router



    -------         -------           -------
    | HA1 |         | FA1 |           | HA2 |
    | R1  |---------| R4  |-----------| R2  |
    |18010|         |18040|           |18020|
    -------        /------\           -------
       |          /        \             |
       |         /          \            |
    -------     /            \        -------
    | R3  |-----              --------| FA2 |
    |18030|                           | R5  |
    -------                           |18050|
                                      -------
  


So, when a node is connecting to router 1 (R1), it will get an IP address
in the range 18011-18019.
  

-----------------------    
* Agent Discovery
-----------------------
When a mobile node connects to a router for the first time, the node 
stores the given address as a home address if the router is a HA.

Then, if the mobile node connects to another router that acts like an
FA, it stores the given address here as a "care-of-address".

Then the mobile node tries to register on this new foreign agent.


-----------------------
* Registration
-----------------------

* Request

The mobile host sends a "Mobile ip ReQuest" (MRQ) message to the FA,
containing home address and newly received "care-of-address", and 
lifetime. Then the hosts starts it's expiry timer.

FA receives the MRQ message, stores the different values in it's 
table, and forwards the MRQ to the home address's router. Then it 
starts it's expiry timer.

HA receives the MRQ message

* Response

HA stores the values in the MRQ messge in it's table, and
sends a "Mobile ip REply" (MRE) message to the mobile node. The
MRQ message contains a status, to tell if registration was 
successfull or not.

The mobile node receives the MRE message, and it can then tell 
whether if registration was successfull or not.

* Expiry

When the mobile nodes registration expiry timer expires, it sends
a new request message.



