// ---------------------------------------------------------------------------------------------------------
// filename:	MRQMessage.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		23.10.2003
// version:		20031023
// description:	Mobile ip ReQuest message. Used by mobile nodes, for requesting registration of care-of-address
//				and home address in foreign agent (FA) and home agent (HA).
//
//				******************** content of message ******************
//				field		- bytesize	-	position	-	content desc
//				***********************************************************
//				identifier	- 	3		-	0..2		-	MRQ		Mobile ip ReQuest message
//				homeAddress -	5		-	3..7		-	The home address of the mobile node
//				careOfAdr	 -	5		-	8..12		-	The care-of-address of the mobile node
//				lifetime 	-	5		-	13..17		-	The lifetime in msec that the registration will be valid
//
// ---------------------------------------------------------------------------------------------------------

package message;

import utility.Debug;


public class MRQMessage {

	private String homeAddress;
	private String careOfAddress;
	private String lifetime;
	Debug d;


	public MRQMessage(String homeAddress_, String careOfAddress_, String lifetime_){
		d				= new Debug("MRQMessage");
		homeAddress		= homeAddress_;
		careOfAddress	= careOfAddress_;
		lifetime		= lifetime_;
	}


	public byte[] flushAsBytes(){
		byte[] bytes = new byte[18];
		int y;
		int index;
		int charvalue;

		// -----------
		// identifier
		// -----------
		bytes[0] = 77; // M
		bytes[1] = 82; // R
		bytes[2] = 81; // Q
		
		
		if ( homeAddress.length()>0 && careOfAddress.length()>0 && lifetime.length()>0 ) {

			// -----------
			// homeAddress
			// -----------
			for (y=0; y<5; y++){
				charvalue = new Integer(homeAddress.substring(y,y+1)).intValue() + 48; // ascii 48 = '0'
				index = 3 + y;
				bytes[index] = (byte)charvalue;
				//d.print("destination.bytes["+index+"]: "+new String(bytes,index,1));
			}
	
			// -----------
			// careOfAddress
			// -----------
			for (y=0; y<5; y++){
				charvalue = new Integer(careOfAddress.substring(y,y+1)).intValue() + 48; // ascii 48 = '0'
				index = 8 + y;
				bytes[index] = (byte)charvalue;
			}
	
			// -----------
			// lifetime
			// -----------
			for (y=0; y<5; y++){
				charvalue = new Integer(lifetime.substring(y,y+1)).intValue() + 48; // ascii 48 = '0'
				index = 13 + y;
				bytes[index] = (byte)charvalue;
			}
		}	
		return bytes;
	}
}
