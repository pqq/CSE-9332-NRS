// ------------------------------------------------------
// filename:	MobileIPDemo.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		15.10.2003
// version:		20031018
// ------------------------------------------------------

import java.util.Date;

import utility.Debug;
import utility.BusyWait;
import node.HomeAgent;
import node.BasicRouter;
import node.ForeignAgent;
import node.MobileNode;
import node.StationaryNode;

public class MobileIPDemo {
	
	public static void main(String argv[]) throws Exception{
		Debug d = new Debug("MobileIPDemo");
		BusyWait busyWait = new BusyWait();
		d.print("Running...");

		
		// ---------------------------------------------------
		// starting the routers
		// ---------------------------------------------------
		d.print("Starting router 1-5");
		HomeAgent		router1 = new HomeAgent("18010");
		HomeAgent		router2 = new HomeAgent("18020");
		BasicRouter		router3 = new BasicRouter("18030");
		ForeignAgent	router4 = new ForeignAgent("18040");
		ForeignAgent	router5 = new ForeignAgent("18050");


		// ---------------------------------------------------
		// wait until all the routing tables will have converged
		// ---------------------------------------------------
		busyWait.doWait(40000);


		// ---------------------------------------------------
		// starting the nodes
		// ---------------------------------------------------
		d.print("Starting one mobile and one stationary node.");
		MobileNode		mn1		= new MobileNode("MN1");
		StationaryNode	cn1		= new StationaryNode("CN1");
		
		
		// ---------------------------------------------------
		// connecting the nodes to the routers
		// ---------------------------------------------------
		d.print("----------------------------------------");
		d.print("Connects mobile node 1 (MN1) to router 1");
		d.print("----------------------------------------");
		mn1.connectTo(router1);
		busyWait.doWait(5000);

		d.print("Connects stationary node 1 (CN1) to router 2");
		cn1.connectTo(router2);
		busyWait.doWait(5000);


		// ---------------------------------------------------
		// sending a data message from CN1 to MN1
		// ---------------------------------------------------
		cn1.sendDataToMN1();
		busyWait.doWait(10000);


		// ---------------------------------------------------
		// move mobile node 1 (MN1) within vicinity of FA1 (R4)
		// ---------------------------------------------------
		d.print("----------------------------------------");
		d.print("Connects mobile node 1 (MN1) to router 4");
		d.print("----------------------------------------");
		mn1.connectTo(router4);


		// ---------------------------------------------------
		// sending a data message from CN1 to MN1
		// ---------------------------------------------------
		cn1.sendDataToMN1();
		busyWait.doWait(20000);
		cn1.sendDataToMN1();
		busyWait.doWait(10000);


		// ---------------------------------------------------
		// move mobile node 1 (MN1) within vicinity of FA2 (R5)
		// ---------------------------------------------------
		d.print("----------------------------------------");
		d.print("Connects mobile node 1 (MN1) to router 5");
		d.print("----------------------------------------");
		mn1.connectTo(router5);


		// ---------------------------------------------------
		// sending a data message from CN1 to MN1
		// ---------------------------------------------------
		busyWait.doWait(20000);
		cn1.sendDataToMN1();
		busyWait.doWait(10000);


		// ---------------------------------------------------
		// move mobile node 1 (MN1) within vicinity of FA1 (R4)
		// ---------------------------------------------------
		d.print("----------------------------------------");
		d.print("Connects mobile node 1 (MN1) to router 4");
		d.print("----------------------------------------");
		mn1.connectTo(router4);


		// ---------------------------------------------------
		// sending a data message from CN1 to MN1
		// ---------------------------------------------------
		busyWait.doWait(20000);
		cn1.sendDataToMN1();
		busyWait.doWait(10000);


		d.print("The best period of my life is NOW, and I enjoy every second...");	
		System.exit(1);
	}
}
