// ----------------------------------------------------------------------------------------
// filename:	BasicRouter.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		22.10.2003
// version:		20031022
// description: Implementation of a basic router (no HA/FA functionality)
// ----------------------------------------------------------------------------------------

package node;

import utility.Debug;
import message.ADMessage;
import message.IPMessage;


public class BasicRouter extends Router{

	private Debug d;


	public BasicRouter(String routerID){
		d = new Debug("BasicRouter '"+routerID+"'");

		d.print("Object created");

		// initialise routing table and set neighbours 
		initialise(routerID);

		// go to running mode
		run();
	}

	
	protected void sendADMessage(String destination){
		ADMessage adMessage = new ADMessage("---");
		IPMessage ipMessage = new IPMessage(getAddress(),destination,adMessage.flushAsBytes());
		sendMessage(ipMessage, this);
	}


	protected void processMRQMessage(String source, String destination, byte[] data){
	}

	protected String getNewDestination(String destination){
		return null;
	}
}
