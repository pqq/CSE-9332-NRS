// ------------------------------------------------------
// filename:	SMNode.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		22.10.2003
// version:		20031022
// description: The parent to all hosts (stationary and mobile hosts)
// ------------------------------------------------------

package node;

import utility.Debug;
import utility.BusyWait;
import node.BasicRouter;
import node.HomeAgent;
import node.ForeignAgent;
import message.ADRMessage;
import message.IPMessage;


abstract public class SMNode extends Node {

	private Debug d;
	private BusyWait bw;
	private String nodeID;


	protected void initialise(String nodeID_){
		nodeID	= nodeID_;
		d		= new Debug("SMNode '"+nodeID+"'");
	}


	public void connectTo(Router router){
		// de-register the host from current connected network
		// note: won't receive de-registration response unless we do a wait here before we stop listener
		sendDeRegistrationMessage();
		
		// stop the listener that is running
		stopListener();

		// receive a new available IPaddress from the router
		String address = router.getNewIPAddress();
		setAddress(address);
		d.print("New IP address received from router: "+address);

		// start listening to this new port
		startListner(address, this, null);

		// give the listener a second to start up before requesting an ADM message (because it's a thread)
		bw.doWait(1000);

		// ask router for ADM message (send an "ADR message")
		requestADMMessage(getAddress());
	}


	public void requestADMMessage(String ipAddress){
		String routerIP = ipAddress.substring(0,4);
		routerIP = routerIP+"0";
		ADRMessage adrMessage = new ADRMessage();
		IPMessage ipMessage = new IPMessage(getAddress(),routerIP,adrMessage.flushAsBytes());
		d.print("Sending an Agent Discovery Request message to '"+routerIP+"'");
		sendMessage(ipMessage, null);
	}


	public String getNodeID(){
		return nodeID;
	}


	protected String getNewDestination(String destination){
		return null;
	}


	abstract public void processMessage(String source, String destination, byte[] data);


	protected abstract void sendDeRegistrationMessage();

}
