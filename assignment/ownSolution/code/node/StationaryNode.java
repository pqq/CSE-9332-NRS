// ------------------------------------------------------
// filename:	StationaryNode.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		23.10.2003
// version:		20031023
// description:	Implementation of a stationary node
// ------------------------------------------------------

package node;

import utility.Debug;
import utility.Message;
import message.IPMessage;
import message.DAMessage;
import message.ACKMessage;


public class StationaryNode extends SMNode {
	private Debug d;
	private Message msg;


	public StationaryNode(String nodeID){
		d	= new Debug("StationaryNode '"+nodeID+"'");
		msg = new Message("StationaryNode '"+nodeID+"'");

		d.print("Object created");

		initialise(nodeID);		
	}
	
	
	public void processMessage(String source, String destination, byte[] data){
		String identifier	= new String(data, 0, 3);
		//d.print("process message from '"+source+"' w/ identifier='"+identifier+"'");

		// Agent Discovery Message
		if (identifier.equals("ADM")){
			// do nothing
		// DAta Message
		} else if (identifier.equals("DAM")){
			String packagebody = new String(data, 3, 1010);
			d.print("Data message received: '"+packagebody.substring(0,12)+"' sending ACKknowledge message back");

			ACKMessage ackMessage = new ACKMessage();
			IPMessage ipMessage = new IPMessage(getAddress(), source, ackMessage.flushAsBytes());
			sendMessage(ipMessage, null);
		} else if (identifier.equals("ACK")){
			d.print("ACKnowledge received");
		} else {
			msg.error("unknown message: '"+identifier+"'");
		}
	}


	protected void sendDeRegistrationMessage(){}

	
	// ------------------------------------------------------------------------------------
	// A condition here is that MN1 has the address "18011" as a home address, it will
	// always get that if it's first connected to router "18010". The address is hardcoded
	// here for testing purposes
	// ------------------------------------------------------------------------------------
	public void sendDataToMN1(){
		d.print("Sending a data message to MN1");
		String destination = "18011";
		String source = getAddress();
		DAMessage daMessage = new DAMessage("Hello World!");
		IPMessage ipMessage = new IPMessage(source, destination, daMessage.flushAsBytes());
		sendMessage(ipMessage, null);
	}


}
